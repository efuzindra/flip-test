# flip-test

Project of Flip Test <br />
This is a simple project of FLIP BackEnd Engineer Test  <br /><br />

Database : MariaDB 10.0  <br /><br />

Project Structure :  <br />
api  <br />
├─── config/  <br />
├────── api_config.php – configuration file used for connecting to the FLIP API.  <br />
├────── db_config.php – configuration file used for connecting to the Database.  <br />
├─── controller/  <br />
├────── check_disburse.php – as main page to check disbursement status which looking for Pending Disburse from DB to be checked  <br />
├────── proses.php – as main page contains logic controller this project  <br />
├────── sent_disburse.php – as main page to run/proses the disburse request which looking for Queue Disburse from DB to be processed  <br />
├─── model/  <br />
├────── conn.php – file that create to DB connection.  <br />
├────── disburse.php – file that contains basic model of disburse data and its functions  <br />
├────── disburselog.php – file that contains basic model of disburse_log table and its functions  <br />
├────── query.php – file that contains basic functions of data to be processes  <br />
├─── migration.php - file to generate the database table  <br />

Running Project :  <br />
command sent disburse (queue) : php check_disburse.php  <br />
command check disburse (pending) : php sent_disburse.php  <br />

written and provided by Fauzi Indra
