<?php

include_once '../model/disburse.php';
include_once '../model/disburselog.php';

class Process{
  function sent_disburse(){
    $config = require_once('../config/api_config.php');

    $conn = new DisburseDataMapper();
    $conn1 = new DisburseLogDataMapper();

    // get all queue data to be sent to disburse API
    $data = $conn->loadAllQueue();
    foreach ($data as $row) {
      $data=array('bank_code'=>$row->bankcode,'account_number'=>$row->accountnumber,'amount'=>$row->amount,'remark'=>$row->remark);
      $options = array(
                  'http'=> array(
                      'method'=>'POST',
                      'header'=>$config['headers'],
                      'protocol_version' => 1.1,
                      'content'=>http_build_query($data)
                  )
      );
      $page=file_get_contents($config['url'],false,stream_context_create($options));

      // insert request/response log
      $conn1->insertData(json_encode($data), $page);

      // update data from data response
      if ($page != '') {
        $resp = json_decode($page, true);
        $conn->updateData($row->id, $resp);
      } else {
        var_dump('the api response is empty');
      }
    }

    return $data;
  }

  function check_disburse(){
    $config = require_once('../config/api_config.php');

    $conn = new DisburseDataMapper();
    $conn1 = new DisburseLogDataMapper();

    // get all pending data to be check its disbursement status
    $data = $conn->loadAllPending();
    foreach ($data as $row) {
      $options = array(
                  'http'=> array(
                      'method'=>'GET',
                      'header'=>$config['headers'],
                      'protocol_version' => 1.1
                  )
      );
      $page=file_get_contents($config['url'].'/'.$row->transactionid,false,stream_context_create($options));

      // insert request/response log
      $conn1->insertData($row->transactionid, $page);

      // update data from data response
      if ($page != '') {
        $resp = json_decode($page, true);
        var_dump($resp);
        $conn->updateDataCheck($row->id, $resp);
      } else {
        var_dump('the api response is empty');
      }
    }
  }
}

?>
