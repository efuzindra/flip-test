<?php
include_once 'query.php';

class DisburseLog
{
    public $id;
    public $req;
    public $res;
    public $timestamp;
    public $created;

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
        $this->req = $data['req'];
        $this->res = $data['res'];
        $this->timestamp = $data['timestamp'];
        $this->created = $data['created'];
    }
}

class DisburseLogDataMapper
{
    private $query;
    private $disburselog;
    private $table_name;
    private $primary_key;

    public function __construct() {
        $this->table_name = 'disburse_log';
        $this->primary_key = 'id';
        $this->query = new Query();
    }

    public function loadQuery($sql)
    {
        $list = array();
        $data = $this->query->query($sql);
        foreach ($data as $row) {
            $list[] = new DisburseLog($row);
        }
        return $list;
    }

    public function loadById($id)
    {
        $data = $this->query->loadSingle($this->table_name, $this->primary_key, $id);
        return new DisburseLog($data);
    }

    public function loadAll()
    {
        $list = array();
        $data = $this->query->loadAll();
        foreach ($data as $row) {
            $list[] = new DisburseLog($row);
        }
        return $list;
    }

    public function insertData($datareq, $datares)
    {

        $sql = 'INSERT INTO '.$this->table_name.' (req, res) VALUES (\''.$datareq.'\',\''.$datares.'\')';

        try{
            $sqlprocess = $this->query->insert($sql);

            if ($sqlprocess === TRUE) {
                echo "New Disburse Log created successfully. \n";
            } else {
                echo "Error: " . $sql . "<br>" . $sqlprocess;
            }
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
    }

}

?>
