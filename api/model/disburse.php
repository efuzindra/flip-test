<?php
include_once 'query.php';

class Disburse
{
    public $id;
    public $bankcode;
    public $accountnumber;
    public $beneficiaryname;
    public $amount;
    public $remark;
    public $status;
    public $receipt;
    public $timeserved;
    public $fee;
    public $transactionid;
    public $timestamp;
    public $created;

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
        $this->id = $data['id'];
        $this->bankcode = $data['bank_code'];
        $this->accountnumber = $data['account_number'];
        $this->beneficiaryname = $data['beneficiary_name'];
        $this->amount = $data['amount'];
        $this->remark = $data['remark'];
        $this->status = $data['status'];
        $this->receipt = $data['receipt'];
        $this->timeserved = $data['time_served'];
        $this->fee = $data['fee'];
        $this->transactionid = $data['transaction_id'];
        $this->timestamp = $data['timestamp'];
        $this->created = $data['created'];
    }
}

class DisburseDataMapper
{
    private $query;
    private $disburse;
    private $table_name;
    private $primary_key;

    public function __construct() {
        $this->table_name = 'disburse';
        $this->primary_key = 'id';
        $this->query = new Query();
    }

    public function loadQuery($sql)
    {
        $list = array();
        $data = $this->query->query($sql);
        foreach ($data as $row) {
            $list[] = new Disburse($row);
        }
        return $list;
    }

    public function loadById($id)
    {
        $data = $this->query->loadSingle($this->table_name, $this->primary_key, $id);
        return new Disburse($data);
    }

    public function loadAll()
    {
        $list = array();
        $data = $this->query->loadAll();
        foreach ($data as $row) {
            $list[] = new Disburse($row);
        }
        return $list;
    }

    public function loadAllQueue()
    {
        $list = array();
        $data = $this->query->query('select * from disburse where status = \'QUEUE\'');
        foreach ($data as $row) {
            $list[] = new Disburse($row);
        }
        return $list;
    }

    public function loadAllPending()
    {
        $list = array();
        $data = $this->query->query('select * from disburse where status = \'PENDING\'');
        foreach ($data as $row) {
            $list[] = new Disburse($row);
        }
        return $list;
    }

    public function updateData($id, $data)
    {
        try{
            $sql = 'UPDATE '.$this->table_name.' SET status = \''.$data['status'].'\', beneficiary_name = \''.$data['beneficiary_name'];
            $sql .= '\', fee = '.$data['fee'].', transaction_id = '.$data['id'].' WHERE id = '.$id;

            $sqlprocess = $this->query->insert($sql);

            if ($sqlprocess === TRUE) {
                echo "New Disburse updated successfully. \n";
            } else {
                echo "Error: " . $sql . "<br>" . $sqlprocess;
            }
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
    }

    public function updateDataCheck($id, $data)
    {
        try{
            $sql = 'UPDATE '.$this->table_name.' SET status = \''.$data['status'].'\', receipt = \''.$data['receipt'];
            $sql .= '\', time_served = \''.$data['time_served'].'\' WHERE id = '.$id;

            $sqlprocess = $this->query->insert($sql);

            if ($sqlprocess === TRUE) {
                echo "Check Disburse updated successfully. \n";
            } else {
                echo "Error: " . $sql . "<br>" . $sqlprocess;
            }
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
    }
}

?>
