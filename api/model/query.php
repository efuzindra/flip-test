<?php
include_once 'conn.php';

class Query
{
    protected $table_name;
    protected $primary_key;

    private $db;

    public function __construct()
    {
        $conn = new Conn();
        $this->db = $conn->getConnection();
    }

    public function query($sql)
    {
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function loadSingle($id)
    {
        $sql = "SELECT * FROM $this->table_name WHERE $this->primary_key = $id";
        return $this->query($sql);
    }

    public function loadAll()
    {
        $sql = "SELECT * FROM $this->table_name";
        return $this->query($sql);
    }

    public function insert($sql)
    {
      if ($this->db->exec($sql)) {
        return TRUE;
      } else {
        die(print_r($this->db->errorInfo(), true));

        return FALSE;
      }
    }
}

?>
