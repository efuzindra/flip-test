<?php

// get config
$config = include('config/db_config.php');

// initialization config
$host = $config['host'];
$db_name = $config['db_name'];
$username = $config['username'];
$password = $config['password'];

$conn = null;

try{
    $conn = new PDO("mysql:host=$host", $username, $password);
    // setting the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Create new Database
    $sql = "CREATE DATABASE IF NOT EXISTS ".$db_name;
    $conn->exec($sql);

    // assing the new database
    $sql = "use ".$db_name;
    $conn->exec($sql);

    // create table disburse
    $sql = "CREATE TABLE `disburse` (
      `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
      `bank_code` varchar(15) NOT NULL,
      `account_number` varchar(15) NOT NULL,
      `beneficiary_name` varchar(15),
      `amount` decimal(15,2) NOT NULL,
      `remark` varchar(255) NOT NULL,
      `transaction_id` varchar(20),
      `status` varchar(255),
      `receipt` varchar(255),
      `time_served` timestamp,
      `fee` decimal(15,2),
      `timestamp` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    )";
    $conn->exec($sql);

    // create table disburse_log
    $sql = "CREATE TABLE `disburse_log` (
      `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
      `req` text DEFAULT NULL,
      `res` text DEFAULT NULL,
      `timestamp` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    )";
    $conn->exec($sql);

    // insert initial (test) record
    $sql = "INSERT INTO `disburse` (`id`, `bank_code`,  `account_number`, `amount`, `remark`, `status`) VALUES
    	(1, 'bni', '1234567890', '10000', 'sample remark', 'QUEUE');";
    $conn->exec($sql);

    echo "Database created successfully with the name ".$db_name;
}catch(PDOException $exception){
    echo "Connection error: " . $exception->getMessage();
}

?>
